#!/usr/bin/python

#
# Copyright (C) 2005 Red Hat, Inc.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#

from gconfvisitor import *

"""Pretty-prints Evolution's account information in a more readable form"""
class EvolutionAccountLogger(EvolutionAccountGConfVisitor):

    def implVisitGroup(self, groupElement):
        print "Group: "
        print "  base_uri:"+ groupElement.getAttribute("base_uri")
        print "       uid:"+ groupElement.getAttribute("uid") 
        print "      name:"+ groupElement.getAttribute("name")
        print "  readonly:"+ groupElement.getAttribute("readonly")
        return True
    
    class ImplMailAccountVisitor(MailAccountVisitor):        
        def visitXml(self, outerVisitor, dom):
            print "Mail Account: " + dom.toprettyxml()
            return True

    class ImplAddressbookSourceVisitor(AddressbookSourceVisitor):
        def visitGroup(self, outerVisitor, groupElement):
            return outerVisitor.implVisitGroup(groupElement)

        def visitSource(self, outerVisitor, groupElement, sourceElement):
            print "Addressbook Source: " + sourceElement.toprettyxml()
            return True
        
    class ImplCalendarSourceVisitor(CalendarSourceVisitor):
        def visitGroup(self, outerVisitor, groupElement):
            return outerVisitor.implVisitGroup(groupElement)

        def visitSource(self, outerVisitor, groupElement, sourceElement):
            print "Calendar Source: " + sourceElement.toprettyxml()
            return True

    class ImplTasksSourceVisitor(TasksSourceVisitor):
        def visitGroup(self, outerVisitor, groupElement):
            return outerVisitor.implVisitGroup(groupElement)

        def visitSource(self, outerVisitor, groupElement, sourceElement):
            print "Tasks Source: " + sourceElement.toprettyxml()
            return True

if __name__ == '__main__':
    gconfEntries = GConfEntriesStdin()
    gconfEntries.accept(EvolutionAccountLogger())
