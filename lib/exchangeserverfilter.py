#!/usr/bin/python

#
# Copyright (C) 2005 Red Hat, Inc.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#

from gconfvisitor import *

"""Filters out all accounts (even local) apart from those on an Exchange server"""
class ExchangeServerFilter(EvolutionAccountGConfVisitor):
    class ImplMailAccountVisitor(MailAccountVisitor):
        def visitXml(self, outerVisitor, dom):
            # Filter out all accounts apart from those on an Exchange server:
            # look for account/source/url of type "exchange"
            return self.getSourceUrlScheme(dom)=="exchange"

    class ImplAddressbookSourceVisitor(AddressbookSourceVisitor):
        def visitSource(self, outerVisitor, groupElement, sourceElement):
            # Filter out all sources apart from on an Exchange server:
            return self.isExchangeGroup(groupElement)
        
    class ImplCalendarSourceVisitor(CalendarSourceVisitor):
        def visitSource(self, outerVisitor, groupElement, sourceElement):
            # Filter out all sources apart from on an Exchange server:
            return self.isExchangeGroup(groupElement)

    class ImplTasksSourceVisitor(TasksSourceVisitor):
        def visitSource(self, outerVisitor, groupElement, sourceElement):
            # Filter out all sources apart from on an Exchange server:
            return self.isExchangeGroup(groupElement)
    
if __name__ == '__main__':
   # load input:
    gconfEntries = GConfEntriesStdin()

    # process it:
    gconfEntries.accept(ExchangeServerFilter())

    # output the result:
    print gconfEntries.dom.toxml()
